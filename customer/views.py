from django.shortcuts import render,get_object_or_404
from django.contrib.auth.models import User
from customer.models import Customer,Order,CustomerQuery,Pizza
from customer.forms import UserForm
from django.contrib.auth import login,logout,authenticate
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


def gettype(request):
    if request.user.is_authenticated() and not request.user.is_superuser:
        customer=Customer.objects.get(user=request.user)
        return customer.type
    else:
        return "None"

def index(request):
    type=gettype(request)
    return render(request,"index1.html",{'type':type})

def registration(request):
    type=gettype(request)
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    if request.method=="POST":
        form=UserForm(data=request.POST)
        if form.is_valid():
            user=form.save()
            user.set_password(user.password)
            user.save()
            customer=Customer(user=user)
            customer.type="customer"
            customer.save()
            return HttpResponseRedirect("/")
        else:
            return render(request,"registration.html",{'form':form,'type':type})
    else:
        form=UserForm()
        return render(request,"registration.html",{'form':form,'type':type})

def user_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    if request.method=="POST":
        username=request.POST["username"]
        password=request.POST["password"]
        # print username,password
        user=authenticate(username=username,password=password)
        print user
        if user:
            login(request,user)
            return HttpResponseRedirect("/")
        else:
            return render(request,"login.html",{'error':'problem in details'})
    else:
        return render(request,"login.html",{'type':type})

def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


def vieworders(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    customer=Customer.objects.get(user=request.user)
    if customer.type!="customer":
        return HttpResponseRedirect("/")
    orders=customer.order_set.all()
    return render(request,"vieworders.html",{'orders':orders,'type':type})

def createorder(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    if request.method=="POST":
        name=request.POST["name"]
        customer=Customer.objects.get(user=request.user)
        order=Order(name=name,orderedby=customer,status=0,checkout=0)
        order.save()
        price=0
        pizzas=Pizza.objects.all()
        checkedpizzas=request.POST.getlist('checks')
        print checkedpizzas
        for pizza in pizzas:
            if pizza.name in checkedpizzas:
                print pizza
                order.pizzas.add(pizza)
                price+=int(pizza.price)
        order.orderprice=price
        order.save()
        print "order created"
        return HttpResponseRedirect("/")
    else:
        pizzas=Pizza.objects.all()
        return render(request,"createorder.html",{'type':type,'pizzas':pizzas})

def cancelorder(request,orderid):
    order=get_object_or_404(Order,id=orderid)
    order.delete()
    return HttpResponseRedirect("/")

def manageorders(request):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="manager":
            return HttpResponseRedirect("/")
        orders=Order.objects.filter(status=0,checkout=1)
        return render(request,"manageorders.html",{'orders':orders,'type':type})
    else:
        return HttpResponseRedirect("/")

def changeorderstatus(request,orderid):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="manager":
            return HttpResponseRedirect("/")
        else:
            order=get_object_or_404(Order,id=orderid)
            order.status=1
            order.save()
            return HttpResponseRedirect(reverse('manageorders'))
    else:
        return HttpResponseRedirect("/")

def submitquery(request):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="customer":
            return HttpResponseRedirect("/")
        else:
            if request.method=="POST":
                description=request.POST["description"]
                customer=Customer.objects.get(user=request.user)
                query=CustomerQuery(querydescription=description,queryby=customer,status=0)
                query.save()
                return HttpResponseRedirect("/")
            else:
                return render(request,"submitquery.html",{'type':type})
    else:
        return HttpResponseRedirect("/")

def managequeries(request):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="support":
            return HttpResponseRedirect("/")
        else:
            queries=CustomerQuery.objects.filter(status=0)
            return render(request,"managequeries.html",{'queries':queries,'type':type})
    else:
        return HttpResponseRedirect("/")

def changequerystatus(request,queryid):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="support":
            return HttpResponseRedirect("/")
        else:
            query=get_object_or_404(CustomerQuery,id=queryid)
            query.status=1
            query.save()
            return HttpResponseRedirect(reverse("managequeries"))
    else:
        return HttpResponseRedirect("/")

def checkout(request,order_id):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="customer":
            return HttpResponseRedirect("/")
        order=get_object_or_404(Order,id=order_id)
        order.checkout=1
        order.save()
        return HttpResponseRedirect(reverse('vieworders'))
    else:
        return HttpResponseRedirect("/")

def viewpizzas(request):
    if request.user.is_authenticated():
        customer=Customer.objects.get(user=request.user)
        if customer.type!="manager":
            return HttpResponseRedirect("/")
        else:
            pizzas=Pizza.objects.all()
            return render(request,"viewpizzas.html",{'pizzas':pizzas})
    else:
        return HttpResponseRedirect("/")

def createpizza(request):
    type=gettype(request)
    if type!="manager":
        return HttpResponseRedirect("/")
    if request.method=="POST":
        name=request.POST["name"]
        price=request.POST["price"]
        pizza=Pizza(name=name,price=price)
        pizza.save()
        return HttpResponseRedirect(reverse("viewpizzas"))
    else:
        return render(request,"createpizza.html",{'type':type})

def deletepizza(request,pizza_id):
    type=gettype(request)
    if type!="manager":
        return HttpResponseRedirect("/")
    pizza=get_object_or_404(Pizza,id=pizza_id)
    pizza.delete()
    return HttpResponseRedirect(reverse("viewpizzas"))
