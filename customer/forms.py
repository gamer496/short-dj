from django import forms
from customer.models import Customer
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password','first_name','last_name')


# class CreateOrder(forms.ModelForm):
#     name            =forms.CharField        (max_length=100)
#
#     class Meta:
#         model=CreateOrder
#         exclude=('orderby','status',)
