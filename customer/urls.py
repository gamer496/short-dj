from django.conf.urls import url
from customer import views

urlpatterns=[
    url(r'^$',views.index,name="customerhome"),
    url(r'registration/',views.registration,name="userregistration"),
    url(r'login/',views.user_login,name="userlogin"),
    url(r'logout/',views.user_logout,name="userlogout"),
    url(r'createorder/',views.createorder,name="createorder"),
    url(r'cancelorder/(?P<orderid>\d+)/',views.cancelorder,name="cancelorder"),
    url(r'vieworders/',views.vieworders,name="vieworders"),
    url(r'manageorders/',views.manageorders,name="manageorders"),
    url(r'changeorderstatus/(?P<orderid>\d+)/',views.changeorderstatus,name="changeorderstatus"),
    url(r'submitquery/',views.submitquery,name="submitquery"),
    url(r'managequeries/',views.managequeries,name="managequeries"),
    url(r'changequerystatus/(?P<queryid>\d+)/',views.changequerystatus,name="changequerystatus"),
    url(r'checkout/(?P<order_id>\d+)/',views.checkout,name="checkout"),
    url(r'viewpizzas/',views.viewpizzas,name="viewpizzas"),
    url(r'createpizza/',views.createpizza,name="createpizza"),
    url(r'deletepizza/(?P<pizza_id>\d+)/',views.deletepizza,name="deletepizza"),
]
