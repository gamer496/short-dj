from django.contrib import admin
from customer.models import Customer,Order,CustomerQuery,Pizza

class CustomerAdmin(admin.ModelAdmin):
    model=Customer
    list_display=['get_name','get_email']

    def get_name(self,obj):
        return obj.user.username
    def get_email(self,obj):
        return obj.user.email
    get_name.admin_order_field='User'
    get_name.short_description='Username'
    get_email.short_description='email'

admin.site.register(Customer,CustomerAdmin)
admin.site.register(Order)
admin.site.register(CustomerQuery)
admin.site.register(Pizza)
