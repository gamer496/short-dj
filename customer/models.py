from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Customer(models.Model):
    user        =models.OneToOneField   (User)
    type        =models.CharField       (max_length=100)

    def __unicode__(self):
        return self.user.username

class Pizza(models.Model):
    name                    =models.CharField       (max_length=100)
    price                   =models.CharField       (default="0",max_length=10)

    def __unicode__(self):
        return self.name

class Order(models.Model):
    name        =models.CharField       (max_length=100)
    status      =models.IntegerField    (default=0)
    checkout    =models.IntegerField    (default=0)
    orderedby   =models.ForeignKey      (Customer,blank=True,null=True)
    pizzas      =models.ManyToManyField (Pizza,blank=True)
    orderprice  =models.IntegerField    (default=0)

    def __unicode__(self):
        return self.name

class CustomerQuery(models.Model):
    querydescription       =models.TextField       ()
    queryby                =models.ForeignKey      (Customer,blank=True,null=True)
    status                 =models.IntegerField    (default=0)

    def __unicode__(self):
        return self.querydescription
